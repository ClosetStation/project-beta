from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .encoders import AutomobileVO, TechnicianEncoder, AppointmentListEncoder, AppointmentDetailEncoder
from .models import AutomobileVO, Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods("GET")
def api_details_technician(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(id=pk)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all().order_by(
            "-date"
        )
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(tech_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_details_appointment(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointments,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def api_vin_appointments(request, vin):
    appointments = Appointment.objects.filter(vin=vin)
    if appointments:
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        response = JsonResponse({"message": "No History found"})
        response.status_code = 400
        return response
