from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    colour = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("import_href", "-year", "vin")


class Technician(models.Model):
    tech_name = models.CharField(max_length=69)
    tech_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.tech_name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=69)
    vip = models.BooleanField(default=False)
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField(max_length=300)
    finished = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician, related_name="appointments",
        on_delete=models.PROTECT, null=True
    )

    def __str__(self):
        return f"{self.customer_name}'s appointment on {self.date}"

    def get_api_url(self):
        return reverse("api_detail_appointment",
        kwargs={"id": self.id})
