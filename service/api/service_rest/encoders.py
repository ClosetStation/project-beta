from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVO(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]

    def get_extra_data(self, o):
        return {"id": o.id}

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["tech_name", "tech_id"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
    ]

    def get_extra_data(self, o):
        return {
            "technician": o.technician.tech_name,
            "vin": o.id,
            "reason": o.reason,
            "vip": o.vip,
            "finished": o.finished,
            "id": o.id,
        }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "vip",
        "vin",
        "date",
        "time",
        "reason",
        "finished",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
