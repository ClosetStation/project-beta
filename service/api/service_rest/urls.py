from django.urls import path
from .views import (
    api_list_technicians,
    api_details_technician,
    api_list_appointments,
    api_details_appointment,
    api_vin_appointments,
)


urlpatterns = [
    path("technicians/", api_list_technicians,
        name="api_list_technicians"),
    path("technicians/<int:id>/", api_details_technician,
        name="api_detail_technician"),
    path("appointments/", api_list_appointments,
        name="api_list_appointments"),
    path("appointments/<int:pk>/", api_details_appointment,
        name="api_detail_appointment",),
    path("appointments/<str:vin>/", api_vin_appointments,
        name="api_vin_appointment",),
]
