from django.contrib import admin
from .models import AutomobileVO, Technician, Appointment


admin.site.register(AutomobileVO)
admin.site.register(Appointment)
admin.site.register(Technician)
