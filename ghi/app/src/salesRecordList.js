import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function SalesRecordList() {
  const [sales, setSales] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  const deleteSaleRecord = async (id) => {
    const response = await fetch(`http://localhost:8090/api/sales/${id}/`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    setSales(
      sales.filter((sale) => {
        return sale.id !== id;
      })
    );
  };
  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/sales/new" className="btn btn-primary btn-lg px-4 gap-3">
          Add a Sale
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer</th>
            <th>Employee</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((record) => {
            return (
              <tr key={record.id}>
                <td>{record.customer.name}</td>
                <td>{record.employee.name}</td>
                <td>{record.automobile.vin}</td>
                <td>${record.price}</td>
                <td>
                  <button onClick={() => deleteSaleRecord(record.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalesRecordList;
