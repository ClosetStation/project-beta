import React from 'react';


class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            customerName: "",
            date: "",
            time: "",
            reason: "",
            vip: false,
            finished: false,
            technician: "",
            technicians: [],
        };

        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleVipChange = this.handleVipChange.bind(this);
        this.handleFinishedChange = this.handleFinishedChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value })
    }

    handleCustomerNameChange(event) {
        const value = event.target.value;
        this.setState({ customerName: value })
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({ date: value })
    }

    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({ time: value })
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }


    handleVipChange = () => {
        const value = this.state.vip
        this.setState({ vip: !value });
    };

    handleFinishedChange = () => {
        const value = this.state.finished
        this.setState({ finished: !value });
    };


    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({ technician: value });
    }

    async componentDidMount() {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.customer_name = data.customerName;

        delete data.customerName;
        delete data.technicians;
        const appointmentUrl = `http://localhost:8080/api/appointments/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            this.setState({
                vin: "",
                customerName: "",
                date: "",
                time: "",
                reason: "",
                finished: "",
                technician: "",
            });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Make an Appointment</h1>
                        <form onSubmit={this.handleSubmit}
                            id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleVinChange}
                                    value={this.state.vin}
                                    placeholder="vin"
                                    required type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCustomerNameChange}
                                    value={this.state.customerName}
                                    placeholder="customerName"
                                    type="text"
                                    name="customerName"
                                    id="customerName"
                                    className="form-control" />
                                <label htmlFor="customerName">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleDateChange}
                                    value={this.state.date}
                                    placeholder="date"
                                    required type="date"
                                    name="date"
                                    id="date"
                                    className="form-control" />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleTimeChange}
                                    value={this.state.time}
                                    placeholder="time"
                                    required type="time"
                                    name="time"
                                    id="time"
                                    className="form-control" />
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleReasonChange}
                                    value={this.state.reason}
                                    placeholder="reason"
                                    required type="text"
                                    name="reason"
                                    id="reason"
                                    className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="form-check-input-floating mb-3">
                                <input onChange={this.handleVipChange}
                                    value={this.state.vip}
                                    placeholder="vip"
                                    type="checkbox"
                                    name="vip"
                                    id="vip"
                                    className="form-check-input-floating mb-3" />
                                <label htmlFor="vip">VIP</label>
                            </div>
                            <div className="form-check-input-floating mb-3">
                                <input onChange={this.handleFinishedChange}
                                    value={this.state.finished}
                                    placeholder="finished"
                                    type="checkbox"
                                    name="finished"
                                    id="finished"
                                    className="form-check-input-floating mb-3" />
                                <label htmlFor="finished">Finished</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleTechnicianChange}
                                    value={this.state.technician}
                                    name="technician"
                                    id="technician"
                                    className="form-select">
                                    <option value="">Choose a technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.tech_id}
                                                value={technician.tech_id}>
                                                {technician.tech_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Book Appointment</button>
                        </form>
                    </div>
                    <div>
                        <a href='http://localhost:3000/appointments/'>
                            <button type='button'>Appointment List</button>
                        </a>
                        <a href='http://localhost:3000/appointments/history/'>
                            <button type='button'>Appointment History</button>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}


export default AppointmentForm
