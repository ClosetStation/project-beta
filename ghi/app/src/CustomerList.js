import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function CustomersList() {
  const [customers, setCustomers] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const deleteCustomer = async (id) => {
    const response = await fetch(`http://localhost:8090/api/customers/${id}/`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    setCustomers(
      customers.filter((customer) => {
        return customer.id !== id;
      })
    );
  };
  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/customers/new" className="btn btn-primary btn-lg px-4 gap-3">
          Add a Customer
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => {
            return (
              <tr key={customer.id}>
                <td>{customer.name}</td>
                <td>{customer.address}</td>
                <td>{customer.phone_number}</td>
                <td>
                  <button onClick={() => deleteCustomer(customer.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default CustomersList;
