import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const deleteAutomobile = async (id) => {
    const response = await fetch(
      `http://localhost:8100/api/automobiles/${id}/`,
      {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    setAutomobiles(
      automobiles.filter((automobile) => {
        return automobile.vin !== id;
      })
    );
  };
  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/automobiles/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Add an Automobile
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => {
            return (
              <tr key={automobile.href}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>
                  <button onClick={() => deleteAutomobile(automobile.vin)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default AutomobilesList;
