import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


function VehicleModelList() {
  const [models, setModels] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const deleteModel = async (id) => {
    const response = await fetch(`http://localhost:8100/api/models/${id}/`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    setModels(
      models.filter((model) => {
        return model.id !== id;
      })
    );
  };

  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/models/new" className="btn btn-primary btn-lg px-4 gap-3">
          Add a Model
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    alt={model.name}
                    style={{ width: "200px" }}
                  />
                </td>
                <td>
                  <button onClick={() => deleteModel(model.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default VehicleModelList;
