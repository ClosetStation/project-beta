import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm'
import SalesPersonList from './SalesPersonList'
import ManufacturerList from './ManufacturerList'
import ManufacturerForm from './ManufacturerForm'
import VehicleModelList from './ModelList'
import VehicleModelForm from './ModelForm'
import AutomobilesList from './AutomobileList'
import AutomobileForm from './AutomobileForm'
import CustomersList from './CustomerList'
import CustomerForm from './CustomerForm'
import SalesRecordList from './SalesRecordList'
import SaleRecordForm from './SaleRecordForm'
import SortSales from './SortSale'
import TechnicianForm from './TechnicianForm'
import TechnicianList from './TechnicianList'
import AppointmentForm from './AppointmentForm';
import AppointmentHistory from './AppointmentHistory';
import AppointmentList from './AppointmentList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers"><Route path="new" element={<ManufacturerForm />} /></Route>
          <Route path="/salesperson" element={<SalesPersonList />} />
          <Route path="salesperson"><Route path="new" element={<SalesPersonForm />} /></Route>
          <Route path="/models"  element={<VehicleModelList />} />
          <Route path="models"><Route path="new" element={<VehicleModelForm />} /></Route>
          <Route path="/automobiles"  element={<AutomobilesList />} />
          <Route path="automobiles"><Route path="new" element={<AutomobileForm />} /></Route>
          <Route path="/customers"  element={<CustomersList />} />
          <Route path="customers"><Route path="new" element={<CustomerForm />} /></Route>
          <Route path="/sales"  element={<SalesRecordList />} />
          <Route path="sales"><Route path="new" element={<SaleRecordForm />} /></Route>
          <Route path="/sortsales"  element={<SortSales />} />
          <Route path="technicians/">
            <Route index element={<TechnicianList />} />
            <Route path='new/' element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentList />} />
            <Route path='new/' element={<AppointmentForm />} />
            <Route path='history/' element={<AppointmentHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
