import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'


function SortSales() {
    const [sales, setSales] = useState([])
    const [salesPerson, setSalesPerson] =  useState([])
    const [employeeID, setEmployeeID] = useState("")
    const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales)
    }
    }
    const getSalesData = async () => {
        const response = await fetch('http://localhost:8090/api/salesperson/');
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.sales_person)
        }
    }
    useEffect(()=>{
    getData()
    getSalesData()
    }, [])

    const filterSales=employeeID ? sales.filter(sale=>sale.employee.employee_id===employeeID) : sales

    return (
        <>
        <div className="container">
            <select value={employeeID} onChange={event => setEmployeeID(event.target.value)}>
                <option value="">Choose an Employee</option>
                {salesPerson.map(seller =><option key= {seller.employee_id} value={seller.employee_id}>{seller.name}</option>)}
            </select>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Customer</th>
            <th>Employee</th>
            <th>Employee ID</th>
            <th>Automobile VIN</th>
            <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {filterSales.map(record => {
            return (
                <tr key={record.id}>
                <td>{ record.customer.name }</td>
                <td>{ record.employee.name }</td>
                <td>{ record.employee.employee_id }</td>
                <td>{ record.automobile.vin }</td>
                <td>${ record.price }</td>
                </tr>
            );
            })}
        </tbody>
        </table>
        </>
    );
}


export default SortSales;
