import React, { useState, useEffect } from "react";


function CustomerForm() {
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    address: "",
    phone_number: "",
  });

  const getData = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const customersUrl = "http://localhost:8090/api/customers/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(customersUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: "",
        address: "",
        phone_number: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,

      [inputName]: value,
    });
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.name}
                required
                type="text"
                name="name"
                id="name"
                className="form-select"
              />
              <label htmlFor="name">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.address}
                required
                type="text"
                name="address"
                id="address"
                className="form-select"
              />
              <label htmlFor="address">Customer Address</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.phone_number}
                placeholder="phone_number"
                required
                type="text"
                name="phone_number"
                id="phone_number"
                className="form-select"
              />
              <label htmlFor="phone_number">Customer Phone Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default CustomerForm;
