import React, { useState, useEffect } from "react";


function SaleRecordForm() {
  const [price, setPrice] = useState("")
  const [customer, setCustomer] = useState("")
  const [automobile, setAutomobile] = useState("")
  const [salesPerson, setSalesPerson] = useState("")
  const [customerList, setCustomerList] = useState([])
  const [automobileList, setAutomobileList] = useState([])
  const [salesPersonList, setSalesPersonList] = useState([])

  const getAutomobileData = async () => {
    const automobileResponse = await fetch(
      "http://localhost:8100/api/automobiles/"
    );
    if (automobileResponse.ok) {
      const data = await automobileResponse.json()
      setAutomobileList(data.autos)
    }
  };

  const getSalesPersonData = async () => {
    const salesPersonResponse = await fetch(
      "http://localhost:8090/api/salesperson/"
    );
    if (salesPersonResponse.ok) {
      const data = await salesPersonResponse.json()
      setSalesPersonList(data.sales_person)
    }
  };

  const getCustomerData = async () => {
    const customerResponse = await fetch(
      "http://localhost:8090/api/customers/"
    );
    if (customerResponse.ok) {
      const data = await customerResponse.json()
      setCustomerList(data.customers)
    }
  };

  useEffect(() => {
    getAutomobileData();
    getCustomerData();
    getSalesPersonData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}
    data.price = price
    data.employee = Number(salesPerson)
    data.customer = Number(customer)
    data.automobile = Number(automobile)
    const salesUrl = "http://localhost:8090/api/sales/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesUrl, fetchConfig)
    if (response.ok) {
      setAutomobile("")
      setSalesPerson("")
      setCustomer("")
      setPrice("")
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={handleSubmit} id="create-a-customer">
            <div className="form-floating mb-3">
              <select
                value={automobile}
                id="auto"
                onChange={(event) => setAutomobile(event.target.value)}
                name="name"
                className="form-control"
              >
                <option value="">Choose an Automobile</option>
                {automobileList.map((auto) => {
                  return (
                    <option value={auto.id} key={auto.id}>
                      {auto.year} / {auto.color} /{" "}
                      {auto.model.manufacturer.name} / {auto.model.name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="autos">Automobile</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={salesPerson}
                id="salesPerson"
                onChange={(event) => setSalesPerson(event.target.value)}
                name="salesPerson"
                className="form-control"
              >
                <option value="">Choose an Sales Person</option>
                {salesPersonList.map((seller) => {
                  return (
                    <option value={seller.id} key={seller.id}>
                      {seller.name} / {seller.employee_id}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="salesPerson">Sales Person</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={customer}
                id="customer"
                onChange={(event) => setCustomer(event.target.value)}
                required
                name="customer"
                className="form-control"
              >
                <option value="">Choose an Customer</option>
                {customerList.map((customer) => {
                  return (
                    <option value={customer.id} key={customer.id}>
                      {customer.name} / {customer.address} /{" "}
                      {customer.phone_number}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => setPrice(event.target.value)}
                placeholder="price"
                required
                type="number"
                min="0"
                name="price"
                id="price"
                className="form-select"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default SaleRecordForm;
