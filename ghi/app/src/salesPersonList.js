import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function SalesPersonList() {
  const [salesPerson, setSalesPerson] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/salesperson/");
    if (response.ok) {
      const data = await response.json();
      setSalesPerson(data.sales_person);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  const deleteSalesPerson = async (id) => {
    const response = await fetch(
      `http://localhost:8090/api/salesperson/${id}/`,
      {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    setSalesPerson(
      salesPerson.filter((salePerson) => {
        return salePerson.id !== id;
      })
    );
  };
  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/salesperson/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Add a Sales Person
        </Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
          </tr>
        </thead>
        <tbody>
          {salesPerson.map((salePerson) => {
            return (
              <tr key={salePerson.id}>
                <td>{salePerson.name}</td>
                <td>{salePerson.employee_id}</td>
                <td>
                  <button onClick={() => deleteSalesPerson(salePerson.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalesPersonList;
