CarCar

Team:

* Luis Velazquez - Sales
* Keyan Rogalsky - Service


*Sales microservice*

Explain your models and integration with the inventory
microservice, here.
These models represent four entities in a database: Automobile, Sales Person, Customer, and Sale. Each class defines fields for storing relevant data about each entity, such as the vin number of an automobile, the name of a sales person, and the phone number of a customer. Additionally, the Sale model has foreign key relationships to the other three models, which creates relationships between the entities in the database. In this case, the ForeignKey fields in the Sale model establish relationships between the Sale model and the Customer, SalesPerson, and AutomobileVO models, allowing the application to easily retrieve and manipulate related data. Therefore, integrating these models with the database, the code can create, read, update, and delete (CRUD) data in the database through the use of the models and the Django ORM (Object-Relational Mapping).  This makes it easier to work with the data in the database and allows for more efficient database operations.

*Service microservice*

Explain your models and integration with the inventory
microservice, here.

The Service models include Technician and Appointment.
Integrates with Inventory with AutomobileVO, which polls data from it using update_or_create function. Includes model fields: colour, year, vin, sold, and import_href.
