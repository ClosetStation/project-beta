from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)
    is_sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    name = models.CharField(max_length=100, unique=False)
    employee_id = models.CharField(max_length=200, unique=True)


class Customer(models.Model):
    name = models.CharField(max_length=200, unique=False)
    address = models.CharField(max_length=200, unique=False)
    phone_number = models.CharField(max_length=200, unique=True)


class Sale(models.Model):
    price = models.PositiveIntegerField()
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE
    )
    employee = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_saleDetail", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.employee} - {self.customer} {self.automobile}"

    class Meta:
        ordering = ("employee", "customer")
