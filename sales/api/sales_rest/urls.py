from django.urls import path
from .views import api_saleDetail, api_salesRecordList, api_salesPerson, api_salePersonDetail, api_customerList, api_customerDetail


urlpatterns = [
    path("sales/", api_salesRecordList, name="api_salesRecordList"),
    path("sales/<int:pk>/", api_saleDetail, name="api_saleDetail"),
    path("salesperson/", api_salesPerson, name="api_salesPerson"),
    path("salesperson/<int:pk>/", api_salePersonDetail, name="api_salePersonDetail"),
    path("customers/", api_customerList, name="api_customerList"),
    path("customers/<int:pk>/", api_customerDetail, name="api_customerDetail"),

]
